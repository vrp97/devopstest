----- Installing Docker & Git using repository -----
Ref -- https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository

sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo docker run hello-world

----- Installing kubectl using repository -----
Ref -- https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-using-native-package-management

sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2 curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
kubectl version --client

----- Installing minikube -----
Ref -- https://minikube.sigs.k8s.io/docs/start/#debian-package

curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
minikube version

---- Installing Helm -----
Ref -- https://helm.sh/docs/intro/install/#from-apt-debianubuntu

curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
helm version

---- Installing JDK -----
sudo apt install default-jdk

---- Install Eclipse -----
https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2020-09/R/eclipse-jee-2020-09-R-linux-gtk-x86_64.tar.gz

---- Install Tomcat ------
https://tomcat.apache.org/download-90.cgi

----- Creating & running docker image to run Java web project -----
FROM tomcat:9.0
WORKDIR /usr/local/tomcat/webapps/
RUN mkdir HelloWorld 
COPY . HelloWorld/
EXPOSE 8080

docker build -t mywebapp

docker run -it -p 8080:8080 mywebapp

localhost:8080/HelloWorld

----- Installing jenkins -----
https://www.jenkins.io/download/
java -jar jenkins.war --httpPort=9090

---- Jenkins file for push image -----
node {
	def customImage = ''
	stage('get git repo'){
	    checkout scm
    }
    stage('creating image'){
   			customImage = docker.build("vrp97/mywebapp:${env.BUILD_ID}","./HelloWorld/WebContent")
   	}
   	stage('pusing imageto docker hub'){
	    docker.withRegistry('', 'dockerHubCred') {
			customImage.push()
		}
	}
}

----- DSLJenkins for run pushed image ---
node {
    def customImage = ''
    stage('fetch image') {
        customImage = docker.image('vrp97/mywebapp:12')
        customImage.pull()
    }
    stage('creating container') {
        customImage.withRun('-p 8080:8080'){c->
            sh "docker logs ${c.id}"
            sh "sleep 20"
            sh "curl -Is http://localhost:8080/HelloWorld/"
            sh "sleep 5"
        }
    }
}

---- Deploying jenkins chart on minikube by helm ----
helm repo add jenkins https://charts.jenkins.io
helm repo update
minikube start
helm install jenkins-helm-k8s jenkins/jenkins

1. Get your 'admin' user password by running:
  printf $(kubectl get secret --namespace default jenkins-helm-k8s -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode);echo
2. Get the Jenkins URL to visit by running these commands in the same shell:
  export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/component=jenkins-master" -l "app.kubernetes.io/instance=jenkins-helm-k8s" -o jsonpath="{.items[0].metadata.name}")
  echo http://127.0.0.1:8080
  kubectl --namespace default port-forward $POD_NAME 8080:8080

3. Login with the password from step 1 and the username: admin

----- install az cli ------
https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-apt#install-with-one-command

---- ARM template created from portal ----

---- Deploying ARM template via cli ----
az login -u $username -p $password
az group create --name ExampleGroup --location "EastUS"
az deployment group create --resource-group ExampleGroup --template-file /fileDir/template.json
